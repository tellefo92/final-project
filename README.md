# OpenCV and Jetank controls in C++
This repo is a showcase of how we can take a low-level approach to controlling the movement of the Jetank, as well as using OpenCV with C++ for image processing.

## Requirements
- C++ 11
- OpenCV (for C++)
- Jetson Nano and Waveshark Jetank

## Usage
Clone repository by running
```sh
git clone https://gitlab.com/tellefo92/final-project
cd final-project
```
Build and run with
```sh
sh build.sh
build/main
```

## Demo
A video demonstrating the functionality can be viewed on [youtube](https://youtube.com/shorts/rhNbeA4QOgU)

## Motivation
After having worked with Python and the python-version of the Jetank controls, <br/>
it seemed like an exciting task to build a library for controlling the tank using C++ on a low level.

Doing this provides us with a lot more direct control of the tanks movement, as well as utilizing the speed of C++ \
to do very fast image processing.

The repository for the header files responsible for controlling the Jetank can be found [here](https://github.com/tellefo92/jetank_cpp), \
as well as in the **src** folder.

## Challenges
I would have really liked to include a Docker image to run this program. However, I could not find any \
publicly available docker images that could successfully run on arm64 architecture, as well as having both \
C++11 and OpenCV. 

