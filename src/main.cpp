#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "Motors.h"
#include "Servos.h"
#include <thread>
#include <vector>
#include <atomic>
#include <chrono>
#include <string>
#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <signal.h>

using namespace cv;

std::string gstreamer_pipeline (int capture_width, int capture_height, int display_width, int display_height, int framerate, int flip_method) {
    return "nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)" + std::to_string(capture_width) + ", height=(int)" +
           std::to_string(capture_height) + ", format=(string)NV12, framerate=(fraction)" + std::to_string(framerate) +
           "/1 ! nvvidconv flip-method=" + std::to_string(flip_method) + " ! video/x-raw, width=(int)" + std::to_string(display_width) + ", height=(int)" +
           std::to_string(display_height) + ", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
}

VideoCapture startCamera()
{
    int capture_width = 1280 ;
    int capture_height = 720 ;
    int display_width = 1280 ;
    int display_height = 720 ;
    int framerate = 60 ;
    int flip_method = 0 ;
 
    std::string pipeline = gstreamer_pipeline(capture_width,
    capture_height,
    display_width,
    display_height,
    framerate,
    flip_method);
    
    VideoCapture cap(pipeline, CAP_GSTREAMER);
    if (system("clear"));
    std::cout << "Camera online" << std::endl;
    return cap;
}

void stopCamera(VideoCapture &cap)
{
    cap.release();
}

void moveCam(Point p, Servos &servos, Motors &tank)
{
    // Servo angle speed per second
    const int SERVOS_DEG_PER_SECOND = 90;
    // Motor turning speed per second (left/right)
    const int MOTORS_DEG_PER_SECOND = 120;
    // Distance from center of frame
    Point offset(p.x - 640, p.y - 360);

    if (offset.x != 0 || offset.y != 0)
    {
        // Stop servos at current servo positions
        ServoData s1 = servos.getServoData(1);
        servos.setServoAngle(1, s1.current_angle);
        ServoData s5 = servos.getServoData(5);
        servos.setServoAngle(5, s5.current_angle);

        // Calculate how much servo has to turn
        int x_angle = s1.current_angle;
        if (std::abs(offset.x) > 500)
        {   
            x_angle += offset.x > 0 ? 19 : -18;
        }
        else if (std::abs(offset.x) > 400)
        {
            x_angle += offset.x > 0 ? 14 : -13;
        }
        else if (std::abs(offset.x) > 250)
        {
            x_angle += offset.x > 0 ? 9 : -8;
        }
        else if (std::abs(offset.x) > 100)
        {
            x_angle += offset.x > 0 ? 4 : -3;
        }
        
        int y_angle = s5.current_angle;
        if (std::abs(offset.y) > 200)
        {
            y_angle += offset.y > 0 ? 5 : -5;
        }
        else if (std::abs(offset.y) > 100)
        {
            y_angle += offset.y > 0 ? 2 : -2;
        }

        int motor_sleep_ms = (std::abs(x_angle) / (double) MOTORS_DEG_PER_SECOND) * 1000;
        if (x_angle > 70)
        {
            std::cout << "Moving right\n";
            tank.moveRight();
            servos.setServoAngle(1, 0);
            std::this_thread::sleep_for(std::chrono::milliseconds(motor_sleep_ms));
            while(true)
            {
                s1 = servos.getServoData(1);
                if (s1.current_speed == 0)
                {
                    break;
                }
            }
            tank.moveStop();
        }
        else if (x_angle < -70)
        {
            tank.moveLeft();
            servos.setServoAngle(1, 0);
            std::this_thread::sleep_for(std::chrono::milliseconds(motor_sleep_ms));
            while(true)
            {
                s1 = servos.getServoData(1);
                if (s1.current_speed == 0)
                {
                    break;
                }
            }
            tank.moveStop();
        }
        else if (std::abs(std::abs(x_angle) - std::abs(s1.current_angle)) > 0)
        {
            servos.setServoAngle(1, x_angle);
            int sleep_ms = std::abs(x_angle / (double) 90 * 1000);
        }
        servos.setServoAngle(5, y_angle);
    }
}

// Initialize camera
VideoCapture cap = startCamera();

// Handler for pressing CTRL+C
void signal_callback_handler(int signum) {
   std::cout << "Caught signal " << signum << std::endl;
   stopCamera(cap);
   // Terminate program
   exit(signum);
}

int main()
{
    // Initialize servos
    Servos servos;
    if(!servos.isConnected())
	{
		std::cout<<"Could not connect to servos"<<std::endl;
		return -1;
	}
    std::cout << "Initializing servo positions..\n";
    for (int i = 1; i < 6; ++i)
    {
        servos.setServoSpeed(i, 1000);
        servos.setServoAngle(i, 0);
    }
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "Servos intialized.\n";
    
    // Initialize motors
    Motors tank;
    tank.setSpeed(1.0);

    // Register signal and signal handler
    signal(SIGINT, signal_callback_handler);

    // Initialize classifier
    CascadeClassifier face_cascade;
    if (!face_cascade.load("data/haarcascades/haarcascade_frontalface_default.xml"))
    {
        std::cout << "--(!) Error loading full body cascade\n";
        return -1;
    }
    
    Mat frame;
    const double scaleFactor = 1.0 / 5.0;

    while (cap.read(frame))
    {   
        if (frame.empty())
        {
            std::cout << "--(!) No captured frame -- Break!\n";
            break;
        }
        Mat image;

        cvtColor(frame, image, COLOR_BGR2GRAY);
        resize(image, image, Size(), scaleFactor, scaleFactor);

        std::vector<Rect> faces;
        std::vector<int> numDetections;
        face_cascade.detectMultiScale(image, faces, numDetections, 1.08);
        
        if (faces.size() > 0)
        {
            int bestIndex = std::distance(numDetections.begin(), 
                                          std::max_element(numDetections.begin(), 
                                                           numDetections.end()));
            Rect bestFace = faces[bestIndex];
            Point bestFaceCenter((bestFace.x + bestFace.width / 2) / scaleFactor, (bestFace.y + bestFace.height / 2) / scaleFactor);

            moveCam(bestFaceCenter, servos, tank);
        }
    }
    return EXIT_SUCCESS;
}